﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectToGuess : MonoBehaviour {

    GameManager GM;
    public Color tempColor;

    public int no;

    [SerializeField] bool TrueAnswer;    
  
    public float timer;

    public bool misclick;
    bool oneTime;
    bool isAnswerTime = false;

    private void Start()
    {
        tempColor = GetComponent<Renderer>().material.color;
        misclick = false;
        //TrueAnswer = false;
        oneTime = true;
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void OnMouseDown()
    {
        if(GameManager.isPlay &&isAnswerTime) clicked();
    }

    public void terpilih()
    {
        print("terpilih");
        GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);
        this.TrueAnswer = true;

        print(TrueAnswer);
    }

    public void clicked()
    {
        print("clicked");
        if (TrueAnswer == true) // kalo jawaban benar
        {
            print("Benar");
            GetComponent<AudioSource>().Stop();
            GetComponent<Renderer>().material.color = new Color(0, 1, 0, 1);
            TrueAnswer = false;
            GM.correctAnswer();
        }
        else // jawaban salah
        {
            misclick = true;
            print("Salah");
            tempColor = GetComponent<Renderer>().material.color;
            GetComponent<Renderer>().material.color = new Color(1, 0, 0, 1);
            GameManager.life -= 1;
            if (GameManager.life <= 0) GetComponent<AudioSource>().Stop();
            GM.showHealth();
            GM.wrongAnswer();
            timer = 2f;
        }
    }

    private void Update()
    {
        if (misclick)
        {
            oneTime = false;
            timer -= Time.deltaTime;
        }
        if (timer <= 0&&!oneTime)
        {
            GetComponent<Renderer>().material.color = tempColor;
            misclick = false;
            oneTime = true;
        }
    }

    public void setAnswerBool(bool answerTime)
    {
        isAnswerTime = answerTime;
    }
}
