﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    [SerializeField] GameObject[] spawner;
    [SerializeField] AudioClip[] clip;
    [SerializeField] AudioClip intro_lvl, game_over, gameover_menu, questionSound;
    [SerializeField] GameObject spawner_choosen;

    AudioSource source;
    [SerializeField] Text Health, gameOver, gameOver_menu, win;

    [SerializeField] float delayForIntroduction = 3f; // bisa diubah di scene unity

    int[] SpawnChecker, ClipChecker;
    int spawned, selectedClip;
    int randomSpawn, randomClip;

    int win_cek;
    public static int life;
    public static bool isPlay, Gameover_menu, isWin;
    static private GameManager instance;

    UnityChanControlScriptWithRgidBody player;
    ObjectToGuess[] guessWhere;

    void Start() {

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<UnityChanControlScriptWithRgidBody>();
        guessWhere = GameObject.FindObjectsOfType<ObjectToGuess>();
        source = GetComponent<AudioSource>();

        isPlay = true;
        Gameover_menu = false;
        isWin = false;
        life = 3;
        win_cek = 0;
        instance = this;


        SpawnChecker = new int[spawner.Length];
        ClipChecker = new int[clip.Length];

        foreach (GameObject spawn in spawner) spawn.SetActive(false);
        for (int i = 0; i < SpawnChecker.Length; i++) SpawnChecker[i] = spawner.Length;
        for (int i = 0; i < ClipChecker.Length; i++) ClipChecker[i] = spawner.Length;

        StartCoroutine(intro());

        showHealth();        
    }

    IEnumerator intro()
    {
        print("player cant answer yet");
        player.setPlayerControl(false);
        yield return new WaitForSeconds(1f);

        //play introduction level sound
        print("play intro sound");
        source.clip = intro_lvl;
        source.loop = false;
        source.Play();

        yield return new WaitForSeconds(delayForIntroduction);

        //activate spawner
        foreach (GameObject spawn in spawner) spawn.SetActive(true);
        //random sound and spawner
        initialize();
    }

    int Randomizer()
    {
        int value = Random.Range(0, spawner.Length);
        return value;
    }

    bool spawnCheck(int valueToCheck, int valueInsert)
    {
        for (int i = 0; i < SpawnChecker.Length; i++)
        {
            if (SpawnChecker[i] == valueToCheck)
                return false;
        }
        SpawnChecker[valueInsert] = valueToCheck;
        spawned += 1;
        return true;
    }

    bool clipCheck(int valueToCheck, int valueInsert)
    {
        for (int i = 0; i < SpawnChecker.Length; i++)
        {
            if (ClipChecker[i] == valueToCheck)
                return false;
        }
        ClipChecker[valueInsert] = valueToCheck;
        selectedClip += 1;
        return true;
    }

    public void showHealth()
    {
        Health.text = life.ToString();
        if (life <= 0)
        {
            isPlay = false;
            Gameover_menu = true;
            gameOver.text = "GAME OVER";
            source.Stop();
            //Object.Destroy(gameOver, 5f);
            //Destroy(gameOver);
            //source.PlayOneShot(game_over, 0.75f);
            source.PlayOneShot(gameover_menu, 0.75f);
            //Gameover_menu = true;
            //gameOver_menu.text = "MAIN LAGI ATAU KELUAR";
        }
    }

    public void wrongAnswer()
    {
        print("Wrong answer");
        for (int i = 0; i < guessWhere.Length; i++) { guessWhere[i].setAnswerBool(false); }
        StartCoroutine(playSoundAndQuestion());
    }

    public void correctAnswer()
    {
        print("Correct answer");
        initialize();
    }

    public void initialize()
    {
        for (int i = 0; i < guessWhere.Length; i++) { guessWhere[i].setAnswerBool(false); }
        StartCoroutine(playSoundAndQuestion());
        print("initialize");

        print("spawner activate");
        foreach (GameObject spawn in spawner) spawn.SetActive(true);

        //player.setPlayerControl(false);
        for (int i = 0; i < guessWhere.Length; i++) {guessWhere[i].setAnswerBool(false);}

        randomSpawn = Randomizer();
        if (spawnCheck(randomSpawn, spawned))
        {
            print(spawner[randomSpawn]);
            win_cek += 1;
            //spawner[randomSpawn].GetComponent<Renderer>().material.color = new Color(0, 0, 1, 1);
            source = spawner[randomSpawn].GetComponent<AudioSource>();
            spawner[randomSpawn].GetComponent<ObjectToGuess>().terpilih();
            //spawner[randomSpawn].GetComponent<ObjectToGuess>().TrueAnswer = true;
            initialClip();
        }
        else
        {
            if (win_cek == spawner.Length)
            {
                isWin = true;
                win.text = "SELAMAT, ANDA MENANG";
                //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                instance.LoadLevel();
            }
            else 
                initialize();
        }        
    }

    private void LoadLevel()
    {
        StartCoroutine( LoadCo() );
    }

    private IEnumerator LoadCo()
    {
        yield return new WaitForSeconds(6f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    void initialClip()
    {        
        randomClip = Randomizer();
        while (!clipCheck(randomClip, selectedClip))
        {
            randomClip = Randomizer();
        }
        //if (clipCheck(randomClip, selectedClip))
        //{
            print(clip[randomClip]);
            source.clip = clip[randomClip];
            source.Play();
            print("playing the guess sound");
        //}
        //else initialClip();
    }

    IEnumerator playSoundAndQuestion()
    {
        print("playing sound to guess");
        initialClip();
        yield return new WaitForSeconds(3f);
        source.Stop();
        source = GetComponent<AudioSource>();
        source.clip = questionSound;
        source.Play();
        print("player control OK");
        for (int i = 0; i < guessWhere.Length; i++) {guessWhere[i].setAnswerBool(true);}
    }
}
