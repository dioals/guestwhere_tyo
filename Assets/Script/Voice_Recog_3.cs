﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;

public class Voice_Recog_3 : MonoBehaviour
{

    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    GameManager GameM;
    public ObjectToGuess objGuest_1;
    public ObjectToGuess objGuest_2;
    public ObjectToGuess objGuest_3;
    // Use this for initialization
    void Start()
    {
        keywords.Add("Front", () =>
        {
            objGuest_1.clicked();
        });
        keywords.Add("Right", () =>
        {
            objGuest_2.clicked();
        });
        keywords.Add("Left", () =>
        {
            objGuest_3.clicked();
        });
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognezerOnPhraseRecognition;
        keywordRecognizer.Start();
    }

    void KeywordRecognezerOnPhraseRecognition(PhraseRecognizedEventArgs args)
    {

        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    
    void Update()
    {

    }
}
