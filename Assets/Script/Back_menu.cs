﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;

public class Back_menu : MonoBehaviour
{

    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    // Use this for initialization
    void Start()
    {
        keywords.Add("Kembali", () =>
        {
            BacktoGame();
        });
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognezerOnPhraseRecognition;
        keywordRecognizer.Start();

    }
    void KeywordRecognezerOnPhraseRecognition(PhraseRecognizedEventArgs args)
    {

        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
    void BacktoGame()
    {
        print("Kembali ke Scene Menu");
        Application.LoadLevel("main_menu");
    }
    // Update is called once per frame
    void Update()
    {

    }
}
