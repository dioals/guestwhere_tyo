﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;

public class MainMenu : MonoBehaviour {

    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    // Use this for initialization
    void Start () {
        keywords.Add("PLay", () =>
        {
            PlayGame();
        });
        keywords.Add("Credit", () =>
        {
            CreditGame();
        });
        keywords.Add("Keluar", () =>
        {
            ExitGame();
        });
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognezerOnPhraseRecognition;
        keywordRecognizer.Start();

    }
    void KeywordRecognezerOnPhraseRecognition(PhraseRecognizedEventArgs args)
    {

        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
    void PlayGame() {
        print("Menuju Next Scene");
        Application.LoadLevel("lvl_1");
    }
    void CreditGame() {
        print("Menuju Kredit Game");
        Application.LoadLevel("credit");
    }
    void ExitGame() {
        print("Keluar Game");
        Application.Quit();

    }

    // Update is called once per frame
    void Update () {
		
	}
}
