﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;

public class GameOver_menu : MonoBehaviour {
    GameManager GM_;
    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    // Use this for initialization
    void Start()
    {
        GM_ = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (GameManager.Gameover_menu == true) {
            
            keywords.Add("Mulai", () =>
            {
                PlayGame();
            });
            keywords.Add("Keluar", () =>
            {
                BacktoMenu();
            });
            keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
            keywordRecognizer.OnPhraseRecognized += KeywordRecognezerOnPhraseRecognition;
            keywordRecognizer.Start();
        }
    }
    void KeywordRecognezerOnPhraseRecognition(PhraseRecognizedEventArgs args)
    {

        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
    void PlayGame()
    {
        print("Restart Scene");
        Application.LoadLevel(Application.loadedLevel);
    }
    void BacktoMenu()
    {
        print("Keluar Game");
        Application.LoadLevel("main_menu");

    }

    // Update is called once per frame
    void Update()
    {

    }
}
